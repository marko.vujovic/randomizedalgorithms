import random
import math
from typing import List

from sequence import Sequence, NUCLEOTIDES, NUCLEOTIDES_DICT


def _neighbors(sequence: Sequence, d: int) -> List[Sequence]:
    """ Get all Sequences which are different from sequence at d positions """
    if d == 0:
        return [sequence.copy()]
    if len(sequence) == 1:
        # Return [Sequence('A'), Sequence('C'), Sequence('G'), Sequence('T')]
        return list(map(lambda e: Sequence([e]), NUCLEOTIDES.copy()))
    neighborhood: List[Sequence] = []
    nucleotides_suffix = sequence.suffix()
    suffix_neighborhood = _neighbors(nucleotides_suffix, d)
    for n in suffix_neighborhood:
        if nucleotides_suffix.hamming_distance(n) < d:
            for nucleotide in NUCLEOTIDES:
                neighborhood.append(Sequence([nucleotide]) + n)
        else:
            neighborhood.append(Sequence([sequence[0]]) + n)
    return neighborhood


def _contains_every(dna: List[Sequence], pattern: Sequence, d: int) -> bool:
    """ Check if every sequence from dna contains pattern with at most d mismatches """
    for sequence in dna:
        if not sequence.contains(pattern, d):
            return False
    return True


def _duplicate_free(patterns: List[Sequence]) -> List[Sequence]:
    pattern_set = []
    for p in patterns:
        exists = False
        for u in pattern_set:
            if set(u.list) == set(p.list):
                exists = True
                break
        if not exists:
            pattern_set.append(p)
    return pattern_set


######################################################################################


def motif_enumeration(dna: List[Sequence], k: int, d: int) -> List[Sequence]:
    patterns = []
    k_sequences: List[Sequence] = []
    for seq in dna:
        k_sequences.extend(seq.subsequences(k))
    for pattern in k_sequences:
        pattern_neighbors = _neighbors(pattern, d)
        for pattern1 in pattern_neighbors:
            if _contains_every(dna, pattern1, d):
                patterns.append(pattern1)
    return _duplicate_free(patterns)


def count(motifs: List[Sequence], pseudo_count=0) -> List[List[int]]:
    """ Returns count matrix for list of motifs """
    t = len(motifs)
    k = len(motifs[0])
    count_matrix = [[], [], [], []]  # A, C, G, T
    for column in range(k):  # 0 - k-1
        for count_matrix_row in count_matrix:
            count_matrix_row.append(pseudo_count)
        for row in range(t):
            cell: str = motifs[row][column]
            count_matrix[NUCLEOTIDES_DICT[cell]][column] += 1
    return count_matrix


def profile(motifs: List[Sequence]) -> List[List[float]]:
    """ Returns profile matrix for list of motifs (use pseudo counts) """
    t = len(motifs)
    profile_matrix = count(motifs, 1)
    return list(map(lambda row: list(map(lambda cell: cell / t, row)), profile_matrix))


def score(motifs: List[Sequence]) -> int:
    """ Returns score for list of motifs """
    t = len(motifs)
    k = len(motifs[0])
    count_matrix = count(motifs)
    current_score = 0
    for col_index in range(k):
        max_col_value = max(list(map(lambda row_index: count_matrix[row_index][col_index], range(4))))
        current_score += (t - max_col_value)
    return current_score


def score_per_column(motifs: List[Sequence]) -> List[int]:
    """ Returns score for list of motifs """
    t = len(motifs)
    k = len(motifs[0])
    count_matrix = count(motifs)
    score_list = []
    for col_index in range(k):
        max_col_value = max(list(map(lambda row_index: count_matrix[row_index][col_index], range(4))))
        score_list.append(t - max_col_value)
    return score_list


def consensus(motifs: List[Sequence]) -> List[str]:
    """ Returns consensus string for list of motifs """
    t = len(motifs)
    k = len(motifs[0])
    count_matrix = count(motifs)
    consensus_list = []
    for col_index in range(k):
        col_as_list = list(map(lambda row_index: count_matrix[row_index][col_index], range(4)))
        max_col_value = max(col_as_list)
        max_col_value_index = col_as_list.index(max_col_value)
        consensus_list.append(NUCLEOTIDES[max_col_value_index])
    return consensus_list


def generate_dna(t: int, n: int, motif: Sequence, d: int) -> List[Sequence]:
    """
    Generates array of Sequences with implanted motif \n
    :param t: number of Sequences to generate
    :param n: length of each Sequence
    :param motif: motif to implant
    :param d: number of mutations to perform on motif
    :return:
    """
    dna = []
    k = len(motif)
    for i in range(t):
        sequence = Sequence.random(n)
        implant = motif.copy()
        implant.mutate(d)
        location = random.randint(0, n - k)
        for j in range(k):
            sequence[location + j] = implant[j]
        dna.append(sequence)
    return dna


def median_string(dna: List[Sequence], k: int) -> Sequence:
    distance = math.inf
    all_k_sequences = Sequence.get_all(k)
    median = ''
    for pattern in all_k_sequences:
        new_distance = pattern.d_list(dna)
        if distance > new_distance:
            distance = new_distance
            median = pattern
    return median


def greedy_motif_search(dna: List[Sequence], k: int) -> List[Sequence]:
    t = len(dna)
    best_motifs = []
    best_motifs_score = math.inf
    for motif in dna[0].subsequences(k):
        motifs = [motif]
        for i in range(1, t):
            profile_matrix = profile(motifs)
            motifs.append(dna[i].profile_most_probable_k_mer(profile_matrix, k))
        motifs_score = score(motifs)
        if motifs_score < best_motifs_score:
            best_motifs = motifs
            best_motifs_score = motifs_score
    return best_motifs


def motifs_fn(profile_matrix: List[List[float]], dna: List[Sequence]) -> List[Sequence]:
    """ Returns collection of k-mers formed by Profile-most probable k-mers in each Sequence from dna """
    k = len(profile_matrix[0])
    return list(map(lambda seq: seq.profile_most_probable_k_mer(profile_matrix, k), dna))


def randomized_motif_search(dna: List[Sequence], k: int) -> List[Sequence]:
    motifs = list(map(lambda seq: seq.random_subsequence(k), dna))
    best_motifs = motifs
    best_motifs_score = score(best_motifs)
    while 1 == 1:
        profile_matrix = profile(motifs)
        motifs = motifs_fn(profile_matrix, dna)
        motifs_score = score(motifs)
        if motifs_score < best_motifs_score:
            best_motifs = motifs
            best_motifs_score = motifs_score
        else:
            return best_motifs


# noinspection PyPep8Naming
def gibbs_sampler(dna: List[Sequence], k: int, N: int) -> List[Sequence]:
    motifs: List[Sequence] = list(map(lambda seq: seq.random_subsequence(k), dna))
    best_motifs = motifs
    best_motifs_score = score(best_motifs)
    t = len(dna)
    for j in range(N):
        i = random.randint(0, t - 1)
        motifs.pop(i)
        profile_matrix = profile(motifs)
        motifs.insert(i, dna[i].profile_random_k_mer(profile_matrix))
        motifs_score = score(motifs)
        if motifs_score < best_motifs_score:
            best_motifs = motifs
            best_motifs_score = motifs_score
    return best_motifs


##############################################################################################################


def print_score_per_column(score_list: List[int]):
    print(' ' * 6, end='')
    for index, s in enumerate(score_list):
        print('{0:>2}  '.format(s), end='')
        if index < len(score_list) - 1:
            print('+ ', end='')
        else:
            print('= ', end='')
            print(sum(score_list))


def print_consensus_string(consensus_string: List[str]):
    print(' ' * 4, end='')
    for c in consensus_string:
        print('{0:>4}  '.format(c), end='')


def print_count_matrix(count_matrix: List[List[int]]):
    for index, row in enumerate(count_matrix):
        print(NUCLEOTIDES[index] + ' : ', end='')
        for cell in row:
            print('{0:>4}, '.format(cell), end='')
        print('')


def print_profile_matrix(profile_matrix: List[List[float]]):
    for index, row in enumerate(profile_matrix):
        print(NUCLEOTIDES[index] + ' : ', end='')
        for cell in row:
            print('{:0.2f}, '.format(cell), end='')
        print('')
