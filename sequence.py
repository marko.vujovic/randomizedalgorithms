import random
import itertools
from typing import List

NUCLEOTIDES = ['A', 'C', 'G', 'T']

NUCLEOTIDES_DICT = {'A': 0, 'C': 1, 'G': 2, 'T': 3}


class Sequence:
    list: List[str]

    def __init__(self, nucleotides_list: List[str]):
        self.list = nucleotides_list

    def __str__(self):
        return ''.join(self.list)

    def __len__(self):
        return len(self.list)

    def __getitem__(self, item):
        return self.list[item]

    def __setitem__(self, key, value):
        self.list[key] = value

    def __delitem__(self, key):
        del self.list[key]

    def __add__(self, other: 'Sequence') -> 'Sequence':
        return Sequence(self.list + other.list)

    def copy(self):
        return Sequence(self.list.copy())

    def mutate(self, mutation_number: int):
        """
        Mutates a sequence \n
        :param mutation_number: number of mutations to perform
        """
        mutation_locations = random.sample(range(len(self)), mutation_number)
        for location in mutation_locations:
            candidates = NUCLEOTIDES.copy()
            candidates.remove(self[location])
            self[location] = random.choice(candidates)

    def hamming_distance(self, sequence: 'Sequence') -> int:
        """ Returns Hamming distance between two Sequences """
        if len(self) != len(sequence):
            raise Exception('Cannot calculate Hamming distance for Sequences of different length')
        cnt = 0
        for i in range(len(self)):
            if self[i] != sequence[i]:
                cnt += 1
        return cnt

    def d(self, text: 'Sequence') -> int:
        """ Returns minimum Hamming distance between sequence and any k-mer in text (k is the length of sequence) """
        k = len(self)
        text_subsequences = text.subsequences(k)
        return min(list(map(lambda s: self.hamming_distance(s), text_subsequences)))

    def d_list(self, dna: List['Sequence']) -> int:
        """ Returns sum of distances between Sequence (self) and all Sequences in dna """
        d = 0
        for seq in dna:
            d += self.d(seq)
        return d

    def contains(self, pattern: 'Sequence', d: int) -> bool:
        """ check if sequence contains pattern with d mismatches at most """
        for i in range(len(self) - len(pattern) + 1):
            mismatches = 0
            for j in range(len(pattern)):
                if self[i + j] != pattern[j]:
                    mismatches += 1
                    if mismatches > d:
                        break
            if mismatches <= d:
                return True
        return False

    def suffix(self) -> 'Sequence':
        """ Returns new Sequence which is same as self without first element """
        return Sequence(self[1:])

    def subsequences(self, k: int) -> List['Sequence']:
        """ Returns all sub-Sequences of length k """
        seq_list = []
        for i in range(len(self) - k + 1):
            seq_list.append(Sequence(self[i: i + k]))
        return seq_list

    def random_subsequence(self, k: int) -> 'Sequence':
        """ Returns random sub-Sequence of length k """
        index = random.randint(0, len(self) - k)
        return Sequence(self[index: index + k])

    def probability(self, profile_matrix: List[List[float]]) -> float:
        """ Returns probability that profile matrix generates Sequence """
        pr = 1
        for col_index, nucleotide in enumerate(self):
            row_index = NUCLEOTIDES_DICT[nucleotide]
            pr *= profile_matrix[row_index][col_index]
        return pr

    def profile_most_probable_k_mer(self, profile_matrix: List[List[float]], k: int) -> 'Sequence':
        """ Returns most probable k-mer that can be generated from profile_matrix """
        most_probable_k_mer = Sequence([])
        max_pr = -1
        for k_mer in self.subsequences(k):
            k_mer_pr = k_mer.probability(profile_matrix)
            if k_mer_pr > max_pr:
                max_pr = k_mer_pr
                most_probable_k_mer = k_mer
        return most_probable_k_mer

    def profile_random_k_mer(self, profile_matrix: List[List[float]]) -> 'Sequence':
        """ Returns random k-mer from Sequence with probability of selecting k-mer Pattern = Pr(Pattern|Profile) """
        k = len(profile_matrix[0])
        all_k_mers = self.subsequences(k)
        probabilities = list(map(lambda k_mer: k_mer.probability(profile_matrix), all_k_mers))
        return random.choices(all_k_mers, weights=probabilities, k=1)[0]

    @staticmethod
    def random(length: int) -> 'Sequence':
        """ Returns random Sequence of nucleotides """
        return Sequence(random.choices(NUCLEOTIDES, k=length))

    @staticmethod
    def from_string(string: str) -> 'Sequence':
        """ Create Sequence from string. String is first converted to upper case """
        return Sequence(list(string.upper()))

    @staticmethod
    def get_all(k: int) -> List['Sequence']:
        """
        Returns list of all possible nucleotide Sequences of length k \n
        Sequence.get_all(3) = ['AAA','AAC','AAG','AAT','ACA','ACC','ACG','ACT','AGA',...,'TTA','TTC','TTG','TTT']
        """
        return list(map(lambda l: Sequence(l), itertools.product(NUCLEOTIDES, repeat=k)))
