from functions import generate_dna, Sequence, motif_enumeration, count, print_count_matrix, profile, \
    print_profile_matrix, score_per_column, print_score_per_column, consensus, print_consensus_string, median_string, \
    greedy_motif_search, randomized_motif_search, score, gibbs_sampler
import math


def test_median_string():
    dna = generate_dna(5, 50, Sequence.from_string('AAAAAA'), 1)
    print('-' * 100)
    print('DNA')
    print('-' * 100)
    for e in dna:
        print(e)
    median = median_string(dna, 6)
    print('-' * 100)
    print('MEDIAN STRING:')
    print('-' * 100)
    print(median)
    print('-' * 100)


def test_motif_enumeration():
    dna = generate_dna(5, 100, Sequence.from_string('AAAAAGGGGG'), 1)
    print('-' * 100)
    print('DNA')
    print('-' * 100)
    for e in dna:
        print(e)
    motifs = motif_enumeration(dna, 10, 1)
    print('-' * 100)
    print('MOTIFS:')
    print('-' * 100)
    for m in motifs:
        print(m)
    print('-' * 100)


def test_score():
    motifs = [
        Sequence.from_string('TCGGGGGTTTTT'),
        Sequence.from_string('CCGGTGACTTAC'),
        Sequence.from_string('ACGGGGATTTTC'),
        Sequence.from_string('TTGGGGACTTTT'),
        Sequence.from_string('AAGGGGACTTCC'),
        Sequence.from_string('TTGGGGACTTCC'),
        Sequence.from_string('TCGGGGATTCAT'),
        Sequence.from_string('TCGGGGATTCCT'),
        Sequence.from_string('TAGGGGAACTAC'),
        Sequence.from_string('TCGGGTATAACC'),
    ]

    print_count_matrix(count(motifs))
    print('-' * 100)

    print_score_per_column(score_per_column(motifs))
    print('-' * 100)

    print_profile_matrix(profile(motifs))
    print('-' * 100)

    print_consensus_string(consensus(motifs))


def test_distance_from_sequence_list():
    dna = [
        Sequence.from_string('TTACCTTAAC'),
        Sequence.from_string('GATATCTGTC'),
        Sequence.from_string('ACGGCGTTCG'),
        Sequence.from_string('CCCTAAAGAG'),
        Sequence.from_string('CGTCAGAGGT'),
    ]
    print(Sequence.from_string('AAA').d_list(dna))


def test_probability():
    profile_matrix = profile([
        Sequence.from_string('TCGGGGGTTTTT'),
        Sequence.from_string('CCGGTGACTTAC'),
        Sequence.from_string('ACGGGGATTTTC'),
        Sequence.from_string('TTGGGGACTTTT'),
        Sequence.from_string('AAGGGGACTTCC'),
        Sequence.from_string('TTGGGGACTTCC'),
        Sequence.from_string('TCGGGGATTCAT'),
        Sequence.from_string('TCGGGGATTCCT'),
        Sequence.from_string('TAGGGGAACTAC'),
        Sequence.from_string('TCGGGTATAACC'),
    ])
    print(Sequence.from_string('ACGGGGATTACC').probability(profile_matrix))
    print(Sequence.from_string('TCGGGGATTTCC').probability(profile_matrix))


def test_greedy_motif_search():
    dna = [
        Sequence.from_string('TTACCTTAAC'),
        Sequence.from_string('GATGTCTGTC'),
        Sequence.from_string('ACGGCGTTAG'),
        Sequence.from_string('CCCTAACGAG'),
        Sequence.from_string('CGTCAGAGGT'),
    ]
    motifs = greedy_motif_search(dna, 4)
    for m in motifs:
        print(m)
    print(consensus(dna))


def test_randomized_motif_search():
    dna = [
        Sequence.from_string('ttACCTtaac'),
        Sequence.from_string('gATGTctgtc'),
        Sequence.from_string('ccgGCGTtag'),
        Sequence.from_string('cactaACGAg'),
        Sequence.from_string('cgtcagAGGT'),
    ]
    best_motifs = []
    best_score = math.inf
    for i in range(100):
        motifs = randomized_motif_search(dna, 4)
        motifs_score = score(motifs)
        if motifs_score < best_score:
            best_score = motifs_score
            best_motifs = motifs
    for m in best_motifs:
        print(m)


def test_randomized_motif_search_2():
    dna = generate_dna(10, 500, Sequence.from_string('AAAAGGGG'), 1)
    print('-' * 100)
    print('DNA')
    print('-' * 100)
    for e in dna:
        print(e)
    best_motifs = []
    best_score = math.inf
    for i in range(500):
        motifs = randomized_motif_search(dna, 8)
        motifs_score = score(motifs)
        if motifs_score < best_score:
            best_score = motifs_score
            best_motifs = motifs
    for m in best_motifs:
        print(m)
    print(consensus(best_motifs))


def test_gibbs_sampler():
    dna = generate_dna(10, 600, Sequence.from_string('AAAAAAAAGGGGGGG'), 4)
    print('-' * 100)
    print('DNA')
    print('-' * 100)
    for e in dna:
        print(e)
    best_motifs = []
    best_score = math.inf
    for i in range(500):
        motifs = gibbs_sampler(dna, 15, 200)
        motifs_score = score(motifs)
        if motifs_score < best_score:
            best_score = motifs_score
            best_motifs = motifs
    for m in best_motifs:
        print(m)
    print(score(best_motifs))
    print(consensus(best_motifs))


if __name__ == '__main__':

    # test_motif_enumeration()

    # test_score()

    # test_distance_from_sequence_list()

    # test_median_string()

    # test_probability()

    # test_greedy_motif_search()

    # test_randomized_motif_search()

    # test_randomized_motif_search_2()

    test_gibbs_sampler()
